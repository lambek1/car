@extends('Car.layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <br/>
                <br/>
                <h3>Добавление данных по автомобилю </h3>
                <a href="/car" class="btn btn-outline-primary">Список</a>
                <a href="/car/create/" class="btn btn-primary">Добавить</a>
                <br/>
                <br/>
                <br/>
                <br/>
                <form action = "/car/create" method = "post">
                    @csrf
                    <div class="form-group row">
                        <label for="model" class="col-sm-2 col-form-label">Модель</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="model" placeholder="Модель" name="model">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="vin" class="col-sm-2 col-form-label">VIN номер</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="vin" placeholder="VIN номер" name="vin">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="reg_number" class="col-sm-2 col-form-label">Гос. номер</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="reg_number" placeholder="Гос. номер" name="reg_number">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="comment" class="col-sm-2 col-form-label">Комментарий</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="comment" placeholder="Комментарий" name="comment"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Создать</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection
