@extends('Car.layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <br/>
                <br/>
                <h3>Информация по автомобилю</h3>
                <a href="/car" class="btn btn-outline-primary ">Список</a>
                <a href="/car/create/" class="btn btn-outline-primary">Добавить</a>
                <a href="/car/{{$data->id}}/edit/" class="btn btn-outline-primary">Редактировать</a>
                <br/>
                <br/>
                <dl class="row">
                    <dt class="col-sm-3">Модель</dt>
                    <dd class="col-sm-9">{{$data->model}}</dd>
                    <dt class="col-sm-3">VIN номер</dt>
                    <dd class="col-sm-9">{{$data->vin}}</dd>
                    <dt class="col-sm-3">Гос. номер</dt>
                    <dd class="col-sm-9">{{$data->reg_number}}</dd>
                    <dt class="col-sm-3">Комментарий</dt>
                    <dd class="col-sm-9">{{$data->comment}}</dd>
                <dl>
            </div>
        </div>
    </div>
@endsection
