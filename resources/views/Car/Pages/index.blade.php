@extends('Car.layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <br/>
                <br/>
                <h3>Информация по автомобилю</h3>
                <a href="/car" class="btn btn-primary">Список</a>
                <a href="/car/create/" class="btn btn-outline-primary">Добавить</a>
                <br/>
                <br/>

                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Модель</th>
                        <th scope="col">VIN номер</th>
                        <th scope="col">Гос. номер</th>
                        <th scope="col">Комментарий</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $e)
                    <tr>
                        <th scope="row">{{$e->id}}</th>
                        <td>{{$e->model}}</td>
                        <td>{{$e->vin}}</td>
                        <td>{{$e->reg_number}}</td>
                        <td>{{$e->comment}}</td>
                        <td>
                            <a href="/car/{{$e->id}}/">Смотреть</a><br/>
                            <a href="/car/{{$e->id}}/edit/">Редактировать</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection
