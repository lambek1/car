<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CarController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view("Car.Pages.index",[
			'data' => DB::table('car')->get()->toArray()
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Request $request) {
		if($request->post('_token')){
//			todo Дописать проверку данных по необходимости
			$id = DB::table('car')->insertGetId([
					'model' => $request->post('model'),
					'vin' => $request->post('vin'),
					'reg_number' => $request->post('reg_number'),
					'comment' => $request->post('comment'),
				]);
			if($id){
				return redirect('/car/'.$id);
			}
		}
		return view("Car.Pages.create");
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		return view("Car.Pages.show",[
			'data' => DB::table('car')->where("id",$id)->get()->first()
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		return view("Car.Pages.edit",[
			'data' => DB::table('car')->where("id",$id)->get()->first()
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request) {
		if ($request->post('_token')) {
//			todo Дописать проверку данных по необходимости
			if (DB::table('car')->where('id', $request->post('id'))->update(['model' => $request->post('model'),
				'vin' => $request->post('vin'),
				'reg_number' => $request->post('reg_number'),
				'comment' => $request->post('comment'),])) {
				return redirect('/car/' . $request->post('id'));
			}
		}
		return view("Car.Pages.edit", ['data' => DB::table('car')->where("id", $request->post('id'))->get()->first(),
			'error' => "Произошла ошибка, обратитесь к администратору"]);

	}

}
